package com.winhilltech.expensetrackerapi.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.winhilltech.expensetrackerapi.security.CustomUserDetailsService;
import com.winhilltech.expensetrackerapi.security.JWTRequestFilter;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private CustomUserDetailsService userDetailsService;

	@Bean
	public JWTRequestFilter authenticationJwtTokenFilter() {
		return new JWTRequestFilter();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http
			.csrf().disable()
			.authorizeRequests()
			.antMatchers("/login", "/register").permitAll()
			.anyRequest().authenticated()
			.and()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		http
		.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
		
		http.httpBasic();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {

		// In memory Authorization Example 1
		/*
		 * auth .inMemoryAuthentication()
		 * .withUser("Anand").password("12345").authorities("admin") .and()
		 * .withUser("Anand4").password("12345").authorities("user") .and()
		 * .passwordEncoder(NoOpPasswordEncoder.getInstance());
		 */

		// In memory Authorization Example 2
		/*
		 * InMemoryUserDetailsManager userDetailsService = new
		 * InMemoryUserDetailsManager();
		 * 
		 * UserDetails user1 =
		 * User.withUsername("Anand").password("12345").authorities("Admin").build();
		 * UserDetails user2 =
		 * User.withUsername("Anand4").password("12345").authorities("Admin").build();
		 * 
		 * userDetailsService.createUser(user1); userDetailsService.createUser(user2);
		 * 
		 * auth.userDetailsService(userDetailsService);
		 */

		// Database Authorization

		auth.userDetailsService(userDetailsService);

	}

	@Bean
	PasswordEncoder passwordEncoder() {
		// return NoOpPasswordEncoder.getInstance();
		return new BCryptPasswordEncoder();
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {

		return super.authenticationManagerBean();
	}
}
