package com.winhilltech.expensetrackerapi.repository;

import java.sql.Date;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.winhilltech.expensetrackerapi.entity.Expense;

@Repository
public interface ExpenseRepository extends JpaRepository<Expense, Long> {

	// SELECT * FROM tbl_expenses WHERE user_id=? AND category=?
	Page<Expense> findByUserIdAndCategory(Long user_id, String category, Pageable page);

	// SELECT * FROM tbl_expenses WHERE user_id=? AND name LIKE '%keyword%'
	Page<Expense> findByUserIdAndNameContaining(Long user_id, String keyword, Pageable page);

	// SELECT * FROM tbl_expenses WHERE user_id=? AND date BETWEEN 'startDate' AND 'endDate'
	Page<Expense> findByUserIdAndDateBetween(Long user_id, Date startDate, Date endDate, Pageable page);

	Page<Expense> findByUserId(Long userId, Pageable page);
	
	Optional<Expense> findByUserIdAndId(Long userId, Long expenseId);
}
