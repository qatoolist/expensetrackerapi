package com.winhilltech.expensetrackerapi.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.winhilltech.expensetrackerapi.entity.AuthModel;
import com.winhilltech.expensetrackerapi.entity.JWTResponse;
import com.winhilltech.expensetrackerapi.entity.User;
import com.winhilltech.expensetrackerapi.entity.UserModel;
import com.winhilltech.expensetrackerapi.security.CustomUserDetailsService;
import com.winhilltech.expensetrackerapi.service.UserService;
import com.winhilltech.expensetrackerapi.utils.JwtTokenUtil;

@RestController
public class AuthController {

	@Autowired
	private UserService userService;

	@Autowired
	private CustomUserDetailsService userDetailsService;
	
	@Autowired
	private AuthenticationManager authrnticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@PostMapping("/login")
	public ResponseEntity<JWTResponse> login(@RequestBody AuthModel authModel) throws Exception {

		authenticate(authModel.getEmail(), authModel.getPassword());
		
		final UserDetails userDetails = userDetailsService.loadUserByUsername(authModel.getEmail());
		
		final String token = jwtTokenUtil.generateToken(userDetails);

		return new ResponseEntity<JWTResponse>(new JWTResponse(token), HttpStatus.OK);
	}

	private void authenticate(String email, String password) throws Exception {
		try {
			authrnticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
		} catch (DisabledException e) {

			throw new Exception("User Disabled");

		} catch (BadCredentialsException e) {

			throw new Exception("Bad Credentials");

		}
	}

	@PostMapping("/register")
	public ResponseEntity<User> register(@Valid @RequestBody UserModel user) {

		return new ResponseEntity<User>(userService.createUser(user), HttpStatus.CREATED);
	}
}
