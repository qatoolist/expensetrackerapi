package com.winhilltech.expensetrackerapi.controller;

import java.sql.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.winhilltech.expensetrackerapi.entity.Expense;
import com.winhilltech.expensetrackerapi.service.ExpenseService;

@RestController
public class ExpenseController {

	@Autowired
	private ExpenseService expenseService;

	@GetMapping("/expenses")
	public Page<Expense> getAllExpenses(Pageable page) {
		return expenseService.getAllExpenses(page);
	}

	@GetMapping("/expenses/category")
	public Page<Expense> getExpensesByCategory(@RequestParam String category, Pageable page) {
		return expenseService.getExpensesByCategory(category, page);
	}

	@GetMapping("/expenses/name")
	public Page<Expense> getExpensesByName(@RequestParam String keyword, Pageable page) {
		return expenseService.getExpensesByName(keyword, page);
	}

	@GetMapping("/expenses/date")
	public Page<Expense> getExpensesByDate(@RequestParam(required = false) Date startDate,
			@RequestParam(required = false) Date endDate, Pageable page) {

		return expenseService.getExpensesByDateRange(startDate, endDate, page);
	}

	@GetMapping("/expenses/{id}")
	public Expense getExpenseById(@PathVariable("id") Long id) {
		return expenseService.getExpenseById(id);
	}

	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	@DeleteMapping("/expenses/{id}")
	public void deleteExpenseById(@PathVariable("id") Long id) {
		expenseService.deleteExpenseById(id);
	}

	@ResponseStatus(value = HttpStatus.CREATED)
	@PostMapping("/expenses")
	public Expense saveExpenseDetails(@Valid @RequestBody Expense expense) {
		return expenseService.saveExpenseDetails(expense);
	}

	@PutMapping("/expenses/{id}")
	public Expense updateExpenseDetails(@RequestBody Expense expense, @PathVariable("id") Long id) {
		return expenseService.updateExpenseDetails(id, expense);
	}

	@GetMapping("/fact/{number}")
	public void calculateFactorial(@PathVariable("number") Long number) throws Exception {

		throw new Exception("General Exception occured!! Have value " + number);
	}
}
