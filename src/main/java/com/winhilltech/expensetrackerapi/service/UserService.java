package com.winhilltech.expensetrackerapi.service;

import com.winhilltech.expensetrackerapi.entity.User;
import com.winhilltech.expensetrackerapi.entity.UserModel;

public interface UserService {

	User createUser(UserModel user);
	User readUser();
	User updateUser(UserModel user);
	void deleteUser();
	User getLoggedInUser();
}
