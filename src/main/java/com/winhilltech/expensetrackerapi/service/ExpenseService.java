package com.winhilltech.expensetrackerapi.service;

import java.sql.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.winhilltech.expensetrackerapi.entity.Expense;

public interface ExpenseService {
	
	Page<Expense> getAllExpenses(Pageable page);
	Page<Expense> getExpensesByCategory(String category, Pageable page);
	Page<Expense> getExpensesByName(String name, Pageable page);
	Page<Expense> getExpensesByDateRange(Date startDate, Date endDate, Pageable page);
	Expense getExpenseById(Long id);
	void deleteExpenseById(Long id);
	Expense saveExpenseDetails(Expense expense);
	Expense updateExpenseDetails(Long id, Expense expense);
	
}
