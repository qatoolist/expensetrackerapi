package com.winhilltech.expensetrackerapi.entity;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class UserModel {

	@NotBlank(message="Name is required.")
	private String name;
	
	@NotNull(message="Email is required.")
	@NotBlank(message="Email is required.")
	@Email(message="invalid Email")
	private String email;
	
	@NotNull(message="Password is required.")
	@Size(min=5, message="Password should be min 5 charactors long")
	private String password;
	
	private Long age = 0L;
	
}
