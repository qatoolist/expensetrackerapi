package com.winhilltech.expensetrackerapi.entity;

import lombok.Data;

@Data
public class ErrorObject {

	private Integer statusCode;
	
	private String message;
	
	private Long timestamp;
}
