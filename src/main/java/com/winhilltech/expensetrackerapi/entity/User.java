package com.winhilltech.expensetrackerapi.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="tbl_users")
public class User {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name= "id")
	private Long id;
	
	@Column(name= "name")
	private String name;
	
	@Column(name="email", unique=true)
	private String email;
	
	@Column(name= "password")
	@JsonIgnore
	private String password;
	
	@Column(name= "age")
	private Long age;
	
	@Column(name= "created_at")
	@CreationTimestamp
	private Timestamp createdAt;
	
	@Column(name= "updated_at")
	@UpdateTimestamp
	private Timestamp updatedAt;
	
}
