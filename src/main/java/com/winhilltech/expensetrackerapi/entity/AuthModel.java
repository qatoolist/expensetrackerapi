package com.winhilltech.expensetrackerapi.entity;

import lombok.Data;

@Data
public class AuthModel {

	private String email;
	
	private String password;
	
}
